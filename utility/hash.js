exports.hashPassword = async (password) => {
  // generate a salt
  const salt = await bcrypt.genSalt(10);
  // hash the password along with our new salt
  const hash = await bcrypt.hash(password, salt);

  return hash;
}