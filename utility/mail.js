var api_key = 'key-7c10c711acf8df9ff5c0d1ec4a7f2a42';
var domain = 'mg.jobboard.ng';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

exports.sendMail = (to, subject, emailBody) => {
  const message = {
      from: 'Librarify <info@librarify.ng>',
      to: to,         // List of recipients
      subject: subject, // Subject line
      html: emailBody //HTML Body
  };

  mailgun.messages().send(message, function (error, body) {
    console.log("body:",body);
  });
}
