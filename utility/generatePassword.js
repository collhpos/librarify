exports.generateRandomCharacter = (length) => {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  
  for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
    return result;
};
  
exports.generateRandomPassword = () => {
  const password = generateRandomCharacter(6);

  return hashPassword(password);
};