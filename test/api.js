let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../controllers/userController");

// Assertion type
chai.should()

chai.use(chaiHttp);

describe('Base API', () =>{
    /**
     * Test the GET route
     */
    describe("/", () =>{
        it("should get all the users", (done) =>{
            chai.request(server)
            .get("/")
            .end((err, response) =>{
                response.should.have.status(200);
                response.should.be.a('array');
                err.should.have.status(500);
            done();
            })
        })
    })
    /**
     * Test the GETALL route
     */

    /**
     * Test the CREATE route
     */

    /**
     * Test the UPDATE route
     */

    /**
     * Test the DELETE route
     */

    /**
     * Test the SEARCH route
     */
})