const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
    name: {
        type: mongoose.Schema.Types.Mixed
    },
    code: {
        type: mongoose.Schema.Types.Mixed
    } 
});

categorySchema.statics = {
    isValid(id) {
       return this.findById(id)
              .then(result => {
                 if (!result) throw new Error('category not found')
       })
    }
};

const Category = mongoose.model('Category', categorySchema);
module.exports = Category;