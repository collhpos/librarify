const mongoose = require('mongoose');

const reviewSchema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    comment:{
        type: String,
        require: true
    },
    book_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Book"
    },
    rating: {
        type: Number,
        default: 0,
        require: true
    }
});

const Review = mongoose.model('Review', reviewSchema);
module.exports = Review;