const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const bookSchema = new mongoose.Schema({
    title: {
        type: String,
        require: [true, 'Please provide your book\'s title'],
        default: 'New Post'    
    },
    category_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Category",
        description: 'Please fill your book\'s category'
    },
    year: {
        type: Number,
        require: [true, 'Please provide the year the book was published']
    },
    pages: {
        type: Number,
        require: [true, 'Please fill the number of pages in the book']
    },
    excerpt: {
        type: String,
        require: [true, 'Please provide a preview of the book']
    },
    views:{
        type: Number,
        default: 0
    },
    author: {
        type: String,
        require: true
    },
    user_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    rating: {
        type: Number,
        default: 0,
        require: true
    },
    book_url:{
        type: String,
        require: true
    },
    thumbnail: {
        type: Buffer
    },
    publish: {
        type: Boolean,
        default: false
    }
},
{
    timestamps: { createdAt: true, updatedAt: false }
});

bookSchema.statics = {
    isValid(id) {
       return this.findById(id)
              .then(result => {
                 if (!result) throw new Error('User not found')
       })
    }
};

bookSchema.plugin(mongoosePaginate);
const Book = mongoose.model('Book', bookSchema);
module.exports = Book;