const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const mongoosePaginate = require('mongoose-paginate-v2');

const userSchema = new mongoose.Schema({
    firstname: {
        type: String,
        require: [true, 'Please fill your firstname']
    },
    lastname: {
        type: String,
        require: [true, 'Please fill your lastname']
    },
    email: {
        type: String,
        require: [true, 'Please fill your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, ' Please provide a valid email']
    },
    password: {
        type: String,
        require: [true, 'Please fill your password'],
        minLength: 6
    },
    roleCode: {
        type: Number,
        require: true,
        min: 1,
        max: 3,
        description: '1 = admin, 2 = author, 3 = reader'
    },
    active: {
        type: Boolean,
        default: false,
        require: true
    },
    date: {
        type: Date,
        default: Date.now,
        require: true
    },
    confirmation_token:{
        type: String
    },
    remember_token: {
        type: String
    }
},
{
    timestamps: { createdAt: true, updatedAt: false }
});

// encrypt the password using 'bcryptjs'
// Mongoose -> Document Middleware
userSchema.pre('save', async function hashPassword(next) {
  try {
    const user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    const salt = await bcrypt.genSalt(10);

    // hash the password along with our new salt
    const hash = await bcrypt.hash(user.password, salt);

    // override the cleartext password with the hashed one
    user.password = hash;
    return next();
  } catch (e) {
    return next(e);
  }
});

// This is Instance Method that is gonna be available on all documents in a certain collection
userSchema.methods.correctPassword = async function (typedPassword, originalPassword) {
    return await bcrypt.compare(typedPassword, originalPassword);
};

userSchema.statics = {
    isValid(id) {
       return this.findById(id)
              .then(result => {
                 if (!result) throw new Error('User not found')
       })
    },
    emailExists(email) {
        return this.findOne({email})
              .then(result => {
                 if (result) throw new Error('Email already exist')
       })
    }
}

userSchema.plugin(mongoosePaginate);
const User = mongoose.model('User', userSchema);
module.exports = User;