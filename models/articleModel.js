const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const articleSchema = new mongoose.Schema({
    title: {
        type: String,
        require: [true, 'Please provide your article\'s title']    
    },
    category_id: {
        type: mongoose.Schema.Types.Mixed,
        ref: "Category"
    },
    body: {
        type: String,
         require: true
    },
    views:  {
        type: Number,
        require: true
    },
    user_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    rating: {
        type: String,
        default: "0",
        require: true
    },
    thumbnail: {
        type: Buffer
    },
    publish: {
        type: Boolean,
        require: true
    }
},
{
    timestamps: { createdAt: true, updatedAt: false }
});

articleSchema.plugin(mongoosePaginate);
const Article = mongoose.model('Article', articleSchema);
module.exports = Article;