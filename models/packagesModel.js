const mongoose = require('mongoose');

const packagesSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    code: {
        type: String
    },
    amount:{
        type: Number,
        require: true
    },
    duration: {
        type: String,
        require: true
    },
    description:{
        type: String,
        require: true
    }
});

packagesSchema.statics = {
    isValid(id) {
       return this.findById(id)
              .then(result => {
                 if (!result) throw new Error('User not found')
       })
    }
};

const Packages = mongoose.model('Packages', packagesSchema);
module.exports = Packages;