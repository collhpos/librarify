const mongoose = require('mongoose');

const subscriptionSchema = new mongoose.Schema({
    package_id: {
        type: mongoose.Schema.Types.Mixed,
        ref: 'Packages'
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    start_date: {
        type: Date,
        require: true
    },
    end_date:{
        type: Date,
        require: true 
    },
    active : {
        type: Boolean,
        default: false,
        required: true
    } 
});

const Subscription = mongoose.model('Subscription', subscriptionSchema);
module.exports = Subscription;