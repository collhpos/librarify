const express = require('express');
const router = express.Router();
const {check} = require('express-validator');
const userSchema = require('../models/userModel');
const categorySchema = require('../models/categoryModel');
const auth = require('../controllers/middlewares/authController');
const book = require('../controllers/bookController');

router.post('/search', book.searchBook)
router.get('/count', auth.protect, book.countBook);

router
    .route('/')
    .get(auth.protect, book.getAllBooks)
    .post([
      check('title', 'Fill in the title of the book').notEmpty(),
      check('category_id').notEmpty().isMongoId().custom(val => categorySchema.isValid(val.toString())),
      check('year', 'Please include the year the book was published').notEmpty().isInt(),
      check('pages', 'Give the number of pages contained in the book').notEmpty().isInt(),
      check('excerpt', 'Give an excerpt on the book').notEmpty(),
      check('author', 'Please fill in the name of the author of the book').notEmpty(),
      check('user_id').notEmpty().isMongoId().custom(val => userSchema.isValid(val.toString()))
    ], auth.protect, book.postBook);

router
    .route('/:id')
    .get(auth.protect, book.getBook)
    .patch(auth.protect, book.updateBook)
    .delete(auth.protect, book.deleteBook);

module.exports = router;