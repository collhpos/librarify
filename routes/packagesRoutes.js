const express = require('express');
const router = express.Router();
const {check} = require('express-validator');
const auth = require('../controllers/middlewares/authController');
const package = require('../controllers/packageController');

router.post('/search', package.searchPackage)

router
    .route('/')
    .get(auth.protect, package.getAllPackages)
    .post([
      check('name', 'Enter the name of the package').notEmpty(),
      check('code', 'Enter the code for the package').notEmpty(),
      check('amount', 'Enter the preferred cost of the package').notEmpty().isInt(),
      check('duration', 'Enter the duration you want the package to be for').optional(['monthly', 'yearly']),
      check('description', 'Give a brief description').notEmpty()
    ], auth.protect, package.postPackage);

router
    .route('/:id')
    .get(auth.protect, package.getPackage)
    .patch(auth.protect, package.updatePackage)
    .delete(auth.protect, package.deletePackage);

module.exports = router;