const express = require('express');
const router = express.Router();
const userSchema = require('../models/userModel');
const bookSchema = require('../models/bookModel');
const {check} = require('express-validator');
const auth = require('../controllers/middlewares/authController');
const review = require('../controllers/reviewsController');

router.post('/search', review.searchReview)

router
    .route('/')
    .get(auth.protect, review.getAllReviews)
    .post([
      check('user_id').isMongoId().custom(val => userSchema.isValid(val.toString())),
      check('comment', 'Please leave a comment').notEmpty(),
      check('book_id').isMongoId().custom(val => bookSchema.isValid(val.toString()))
    ], auth.protect, review.postReview);

router
    .route('/:id')
    .get(auth.protect, review.getReview)
    .patch(auth.protect, review.updateReview)
    .delete(auth.protect, review.deleteReview);

module.exports = router;