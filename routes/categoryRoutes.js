const express = require('express');
const router = express.Router();
const {check} = require('express-validator');
const auth = require('../controllers/middlewares/authController');
const category = require('../controllers/categoryController');

router.post('/search', category.searchCategory)

router
    .route('/')
    .get(auth.protect, category.getAllCategories)
    .post([
      check('name', 'Fill in the name of the category').notEmpty(),
      check('code', 'Fill in the code for the category').notEmpty()
    ], auth.protect, category.postCategory);

router
    .route('/:id')
    .get(auth.protect, category.getCategory)
    .patch(auth.protect, category.updateCategory)
    .delete(auth.protect, category.deleteCategory);

module.exports = router;