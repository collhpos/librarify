const express = require('express');
const router = express.Router();
const userSchema = require('../models/userModel');
const packagesSchema = require('../models/packagesModel');
const {check} = require('express-validator');
const auth = require('../controllers/middlewares/authController');
const subscription = require('../controllers/subscriptionController');

router.post('/search', subscription.searchSubscription)

router
    .route('/')
    .get(auth.protect, subscription.getAllSubscriptions)
    .post([
      check('package_id', 'Fill in valid parent id').isMongoId().custom(val => packagesSchema.isValid(val.toString())),
      check('user_id', 'Fill in a valid user id').isMongoId().custom(val => userSchema.isValid(val.toString())),
      check('start_date', 'Please include the start date').notEmpty(),
      check('end_date', 'Please include the end date').notEmpty()
    ], auth.protect, subscription.postSubscription);

router
    .route('/:id')
    .get(auth.protect, subscription.getSubscription)
    .patch(auth.protect, subscription.updateSubscription)
    .delete(auth.protect, subscription.deleteSubscription);

module.exports = router;