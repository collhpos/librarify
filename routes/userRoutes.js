const express = require('express');
const router = express.Router();
const {check} = require('express-validator');
const Userschema = require('../models/userModel');
const user = require('../controllers/userController');
const auth = require('../controllers/middlewares/authController');

router.post('/signup', [
  check('firstname', 'please fill in your first name').notEmpty().trim().isLength({max: 256}).withMessage('You have exceeded the maximum number of characters'),
  check('lastname', 'please fill in your last name').notEmpty().trim().isLength({max: 256}).withMessage('You have exceeded the maximum number of characters'),
  check('email').notEmpty().exists().isEmail().custom(val => Userschema.emailExists(val)).isLength({max: 255}),
  check('password', 'password should not be lesser than 6 letters nor greater than 15').notEmpty().isLength({ min: 6, max: 255}),
  check('roleCode', 'Please add a roleCode where admin = 1, author = 2, reader = 3').notEmpty().exists().withMessage('Enter your role code').isInt().withMessage('The role code is a number')
], user.signup);

router.post('/admin/create_user', [
  check('firstname', 'please fill in your first name').notEmpty().trim().isLength({max: 256}).withMessage('You have exceeded the maximum number of characters'),
  check('lastname', 'please fill in your last name').notEmpty().trim().isLength({max: 256}).withMessage('You have exceeded the maximum number of characters'),
  check('email').notEmpty().exists().isEmail().custom(val => Userschema.emailExists(val)),
  check('roleCode', 'Please add a roleCode where admin = 1, author = 2, reader = 3').notEmpty().exists().withMessage('Enter your role code').isInt().withMessage('The role code is a number')
], user.create_user);

router.post('/login', [
  check('email', 'Enter your email').notEmpty().isEmail().withMessage('Enter a valid email address').isLength({max: 255}).withMessage('You have exceeded maximum number for password'),
  check('password', 'input password').notEmpty().isLength({ min: 6, max: 255}).withMessage('You have exceeded maximum number for password'),
], user.login);

router.post('/search', auth.protect, user.searchUser);

router.get('/logout', auth.protect, user.logout);
router.get('/count', auth.protect, user.countUser);

router.get('/forgot_password', user.forgot_password);

router
    .route('/')
    .get(auth.protect, user.getAllUsers);

router.patch('/confirm_account/:random_character', user.confirm_account);

router.patch('/reset_password/:random_character', [
  check('email').exists().isEmail().custom(val => Userschema.emailExists(val))
], user.reset_password);

router
    .route('/:id')
    .get(auth.protect, user.getUser)
    .patch(auth.protect, user.updateUser)
    .delete(auth.protect, user.deleteUser);

module.exports = router;