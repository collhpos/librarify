const express = require('express');
const router = express.Router();
const {check} = require('express-validator');
const userSchema = require('../models/userModel');
const categorySchema = require('../models/categoryModel');
const auth = require('../controllers/middlewares/authController');
const article = require('../controllers/articleController');

router.post('/search', article.searchArticle);
router.get('/count', auth.protect, article.countArticle);

router
    .route('/')
    .get(auth.protect, article.getAllArticles)
    .post([
      check('title', 'Fill in the title of the book').notEmpty(),
      check('category_id').notEmpty().isMongoId().custom(val => categorySchema.isValid(val.toString())),
      check('body', 'The content of the article is needed').notEmpty(),
      check('user_id').notEmpty().isMongoId().custom(val => userSchema.isValid(val.toString()))
    ], auth.protect, article.postArticle);

router
    .route('/:id')
    .get(auth.protect, article.getArticle)
    .patch(auth.protect, article.updateArticle)
    .delete(auth.protect, article.deleteArticle);

module.exports = router;