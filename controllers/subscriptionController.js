const Subscription = require('../models/subscriptionModel'),
        {validationResult} = require('express-validator'),
        base = require('./baseController');
    
exports.postSubscription = base.createOne(Subscription);
exports.getSubscription = base.getOne(Subscription);
exports.getAllSubscriptions = base.getAll(Subscription);
exports.updateSubscription = base.updateOne(Subscription);
exports.deleteSubscription = base.deleteOne(Subscription);
exports.searchSubscription = base.search(Subscription);