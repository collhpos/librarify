const Category = require('../models/categoryModel'),
    base = require('./baseController');
    
exports.postCategory = base.createOne(Category);
exports.getCategory = base.getOne(Category);
exports.getAllCategories = base.getAll(Category);
exports.updateCategory = base.updateOne(Category);
exports.deleteCategory = base.deleteOne(Category);
exports.searchCategory = base.search(Category);