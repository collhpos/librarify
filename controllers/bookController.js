const Book = require('../models/bookModel'),
    base = require('./baseController'),
    APIFeatures = require('../utils/apiFeatures');

    
exports.postBook = base.createOne(Book);
exports.getBook = base.getOne(Book);

exports.getAllBooks = async (req, res, next) => {
    try {
        const features = new APIFeatures(Book.find(), req.query)
            .sort()
            .paginate();

        const book = await features.query;

            res.status(200).json({
                            status: 'success',
                            results: book.length,
                            data: book
                        });
    } catch (error) {
        next(error);
    }
};

exports.countBook = base.count(Book);
exports.updateBook = base.updateOne(Book);
exports.deleteBook = base.deleteOne(Book);
exports.searchBook = base.search(Book);