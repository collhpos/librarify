const Review = require('../models/reviewsModel'),
    base = require('./baseController');
    
exports.postReview = base.createOne(Review);
exports.getReview = base.getOne(Review);
exports.getAllReviews = base.getAll(Review);
exports.updateReview = base.updateOne(Review);
exports.deleteReview = base.deleteOne(Review);
exports.searchReview = base.search(Review);