const Article = require('../models/articleModel'),
    base = require('./baseController');
    
exports.postArticle = base.createOne(Article);
exports.getArticle = base.getOne(Article);
exports.getAllArticles = base.getAll(Article);
exports.countArticle = base.count(Article);
exports.updateArticle = base.updateOne(Article);
exports.deleteArticle = base.deleteOne(Article);
exports.searchArticle = base.search(Article);