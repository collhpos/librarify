const {
    promisify
} = require('util');
const jwt = require('jsonwebtoken');
const User = require('../../models/userModel');
const AppError = require('../../utils/appError');

exports.protect = async (req, res, next) => {
    try {
        // 1) check if the token is there
        let token;
        if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
            token = req.headers.authorization.split(' ')[1];
        }
        if (!token) {
            return next(new AppError(401, 'fail', 'You are not logged in! Please login in to continue'), req, res, next);
        }

        // 2) Verify token 
        const decode = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
        if(!decode) {
            return new AppError(401, 'forbidden');
        };

        // 3) check if the user exist (not deleted)
        const user = await User.findById(decode.id);
        if (!user) {
            return next(new AppError(401, 'fail', 'This user is no longer exist'), req, res, next);
        }

        req.user = user;
        req.token = token;
        next();

    } catch (err) {
        next(err);
    }
};

// Authorization check if the user have rights to do this action
exports.restrictTo = (...roleCodes) => {
    return (req, res, next) => {
        if (!roleCodes.includes(req.user.roleCode)) {
            return next(new AppError(403, 'fail', 'You are not allowed to do this action'), req, res, next);
        }
        next();
    };
};