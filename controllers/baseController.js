const AppError = require('../utils/appError');
const  {validationResult} = require('express-validator');

exports.deleteOne = Model => async (req, res, next) => {
    try {
        const data = await Model.findByIdAndDelete(req.params.id);
        const file = data && data.id
        if (file !== req.params.id) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        res.status(200).json({
            status: 'This document has been deleted'
        });
    } catch (error) {
        next(error);
    }
};

exports.updateOne = Model => async (req, res, next) => {
    try {
        const data = await Model.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        });
        const file = data && data.id
        if (file !== req.params.id) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        };

        res.status(200).json({
            status: 'updated',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.createOne = Model => async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    try {
        const data = await Model.create(req.body);

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.getOne = Model => async (req, res, next) => {
    try {
        const data = await Model.findById(req.params.id);
        const file = data && data.id
        if (file !== req.params.id) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        res.status(200).json({
            status: 'success',
            data
        });
    } catch (error) {
        next(error);
    }
};

exports.getAll = Model => async (req, res, next) => {
    try {
        const data = await Model.find();

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }

};

exports.count = Model => async (req, res, next) => {
    try {
        const data = await Model.estimatedDocumentCount({});

        if(data > 0){
            res.status(200).json({
            status: 'success',
            data
            });
        }
        else if(data === 0){
            res.status(200).json({
                status: 'This storage is empty'
            })
        }
    } catch (error) {
        next(error);
    }

};

exports.search = (Model, config=null) => async (req, res, next) => {
    try {
        var page = (req.body.page) ? req.body.page : 1;
        var perPage = (req.body.limit) ? req.body.limit :10;
        var query = req.body.query || {};

        var options = {
            sort: req.body.sort || {createdAt: -1},
            lean: true,
            page: page,
            limit: perPage
        };

        if(config && config.populate) {
            options.populate = config.populate;
        }

        const data = await Model.paginate(query, options);

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }

};
