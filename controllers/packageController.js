const Package = require('../models/packagesModel'),
    base = require('./baseController');
    
exports.postPackage = base.createOne(Package);
exports.getPackage = base.getOne(Package);
exports.getAllPackages = base.getAll(Package);
exports.updatePackage = base.updateOne(Package);
exports.deletePackage = base.deleteOne(Package);
exports.searchPackage = base.search(Package);