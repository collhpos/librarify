const {promisify} = require('util');
const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const Email = require('../utility/mail');
const Hash = require('../utility/hash');
const AutoPassword = require('../utility/generatePassword');
const { uuid } = require('uuidv4');
const base = require('./baseController');
const AppError = require('../utils/appError');
const {validationResult} = require('express-validator');


 const createToken = id => {
    return jwt.sign({id}, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
};

const generateToken = async () => {  
    try{
      const token = uuid();    
      const model = await User.findOne({confirmation_token: token});
      if(model) {
        this.generateToken();
      }
    
      return token;
    }
    catch(error) {
      return error;
    }    
};

exports.signup = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    try {
        const confirmationToken = await generateToken();

        let body = req.body
          body.confirmation_token = confirmationToken ;       
        
        const user = await User.create(body);

        const emailBody = '<p>Hello '+user.firstname+',</p><p>Welcome to Librarify.</p><p> We are glad to have you onboard.</p><p>Please kindly click on the link below to activate your account.</p><p><a href="'+process.env.BASE_URL+'user/confirm_account/'+confirmationToken+'">Accout Activation Link<a></p>'; // HTML body
        const subject = 'Welcome to Librarify!';

        // Mail User
        await Email.sendMail(user.email, subject, emailBody);

        // Remove the password from the output 
        user.password = undefined;

        res.status(200).json({
            message: 'User created successfully',
            status: 'success',
                    data: {
                        user
                    }
             });
    } catch (err) {
        next(err);
    }
};

exports.login = async (req, res, next) => {
     const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    try {
        const {
            email,
            password,
            roleCode
        } = req.body;

        if (!email || !password) {
          return next(new AppError(404, 'fail', 'Please provide email or password'), req, res, next);
        }

        const user = await User.findOne({'email': req.body.email});

        if (!user && !await user.correctPassword(password, user.password)) {
          return next(new AppError(404, 'fail', 'Email or Password is wrong'), req, res, next);
        }
        const file = user && user.roleCode;
        if(file !== roleCode) {
          return next(new AppError(404, 'The role inputted doesn\'t match the role used in creating this account'))
        }

        const token = createToken(user._id);

        if(user) {
            req.session.user_id = user._id

                // Remove the password from the output 
                user.password = undefined;
                
                res.status(200).json({
                    status: 'success',
                    token,
                    user
                });
            }
        } catch (err) {
            next(err);
        }
};

exports.confirm_account = async(req, res, next) => {
    try{  

        if(!req.params.random_character) {
          return next(new AppError(404, 'fail', 'You are not authorized to perform this operation'), req, res, next);
        };

        const data = await User.findOneAndUpdate({confirmation_token: req.params.random_character}, {$set: {active: true}}, {
          new: true,
          runValidators: true,
        });

        const file = data && data.confirmation_token
        if (file !== req.params.random_character) {
          return next(new AppError(404, 'fail', 'This user token doesn\'t exist'), req, res, next);
        };

        let token = {};
        if(file.roleCode === 3) {
            token = createToken(file.id)
        }

        res.status(200).json({
            status: 'your account has been activated successfully',
            data,
            token
        });
        
    }catch(err) {
        next(err);
    }
};

exports.logout = async (req, res, next) => {
    try {
        const logout = req.session.destroy();
        if(!logout) {
          return next(new AppError(404, 'fail', 'unable to logout from this account'), req, res, next);
        }
        const cookie = res.clearCookie(process.env.SESS_NAME);
        if(!cookie) {
          return next(new AppError(404, 'fail', 'unable to clear cookie from this account'), req, res, next);
        }
        res.status(200).json({
            status: 'you have successfully logged out of your account'
        })

    }catch (err) {
        next(err);
    }
};

exports.countUser = base.count(User);

exports.create_user = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {
        const confirmationToken = await generateToken();
        const temporaryPassword = AutoPassword.generateRandomPassword(6);

        let body = req.body
          body.password = await Hash.hashPassword(temporaryPassword);

        const user = await User.create(body);
        const emailBody = '<p>Hello '+user.firstname+',</p><p>An account has just been created with your email address</p><p>You are one step away from joining the Librarify swarm</p><p>kindly click on the link below to change password and then access your account to become a librarifier.</p><p>Your Temporary Password is: '+temporaryPassword +'</p><p><a href="'+process.env.BASE_URL+'/user/reset_password/'+confirmationToken+'">Accout Activation Link<a></p>'; // HTML body
        const subject = 'Welcome to Librarify!';

        // Mail User
        await Email.sendMail(user.email, subject, emailBody);

        res.status(200).json({
          status: 'success',
          user
        });

    } catch (error) {
        next(error);
    }
};
exports.getAllUsers = base.getAll(User);
exports.getUser = base.getOne(User);

exports.updateUser = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };

    if(req.params.id !== req.body._id) {
      return next(new AppError(404, 'Not acceptable', 'the route id does not match the supplied id'), req, res, next);
    };

    if(req.body.password) {
      req.body.password = await Hash.hashPassword(req.body.password);
    };
    
    try {
        const user = await User.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true, runValidators: true}, ); 

        if (!user) {
          return next(new AppError(404, 'Not found', 'No document found with that id'), req, res, next);
        };

        res.status(200).json({
          status: 'updated',
          user
        }); 

    } catch (error) {
      next(error);
    }           
};

exports.forgot_password = async (req, res, next) =>{
    try{
        const random_character = await generateToken();
        const user = await User.findOneAndUpdate({email:req.query.email}, {$set: {remember_token: random_character}}, {
          new: true,
          runValidators: true,
        });

        if(user.remember_token !== random_character){
          return next(new AppError(404, 'fail', 'The token provided does not exist'), req, res, next);
        };

        if(!user){
          return next(new AppError(404, 'fail', 'The email does not exist'), req, res, next);
        };

        const message = {
            from: 'Librarify <info@Librarify.ng>',
            to: user.email,         // List of recipients
            subject: 'Password Reset Request', // Subject line
            html: '<p>Hello '+user.firstname+',</p><p> You recently requested to change your password.</p><p> If you did not make this request, kindly ignore this email.</p><p>To change your password, click on the link below</p><p><a href="'+process.env.BASE_URL+'user/reset_password/'+random_character+'">Reset Password Link<a></p>' // HTML body
        };  

        // Mail User
        await Email.sendMail(message);

        res.status(200).json({
          status: 'A reset link has been sent to your email'
        });

    }catch(err){
        next(err);
    }
};

exports.reset_password = async (req, res, next) =>{
    try{
        if(!req.body.password) {
            return next(new AppError(404, 'fail', 'Password field is required'), req, res, next);
        };

        if(!req.params.random_character) {
            return next(new AppError(404, 'fail', 'You are not authorized to perform this operation'), req, res, next);
        };

        const hash = await Hash.hashPassword(req.body.password);
        const data = await User.findOneAndUpdate({remember_token: req.params.random_character}, {$set: {password: hash}}, {
            new: true,
            runValidators: true,
        });

        if (data.remember_token !== req.params.random_character) {
            return next(new AppError(404, 'fail', 'This user token doesn\'t exist'), req, res, next);
        };

        res.status(200).json({
            status: 'your password has been reset successfully',
            data
        });
    }catch(err) {
        next(err);
    }
};

exports.deleteUser = base.deleteOne(User);
exports.searchUser = base.search(User);