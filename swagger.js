const { postUser, createAuthor, getUsers, confirmAccount, countUsers, searchUser, updateUser, forgotPassword, getUser, resetPassword, deleteUser, loginUser, logoutUser} = require('./openApi/user.swagger'),
    {getPackages, getPackage,  postPackage, searchPackage, updatePackage, deletePackage} = require('./openApi/packages.swagger'),
    {getReviews, getReview, postReview, searchReview, updateReview, deleteReview} = require('./openApi/reviews.swagger'),
    {getCategories, getCategory, postCategory, searchCategory, updateCategory, deleteCategory} = require('./openApi/category.swagger'),
    {getBooks, getBook, postBook, countBooks, searchBook, updateBook, deleteBook} = require('./openApi/book.swagger'),
    {getArticles, getArticle, countArticles, postArticle, searchArticle, updateArticle, deleteArticle} = require('./openApi/article.swagger'),
    {getSubscriptions, getSubscription, postSubscription, searchSubscription, updateSubscription, deleteSubscription} = require('./openApi/susbscription.swagger');

const {definitions} = require('./openApi/definitions');

module.exports = {
    openapi: '3.0.1',
    info: {
        version: '1.0.0',
        title: 'Librarify API Documetation',
        description: 'Librarify API Documetation',
        contact: {
            name: 'David Akpan',
            email: 'praisedavid787@gmail.com',
        },
        license: {
            name: 'Apache 2.0',
            url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
        }
    },
    servers: [
        {
            url: 'http://localhost:5000/api/v1/',
            description: 'Local server'
        },
        {
            url: 'http://librarify-api.herokuapp.com/api/v1/',
            description: 'Local server'
        }
    ],
    tags: [
        {
            name: 'User'
        },
        {
            name: 'Subscription'
        },
        {
            name: 'Review'
        },
        {
            name: 'Package'
        },
        {
            name: 'Category'
        },
        {
            name: 'Book'
        },
        {
            name: 'Article'
        }
    ],
    schemes: [
        "https",
        "http"
    ],
    paths: {
        "/user": {
            get: getUsers
        },
        "/user/count": {
            get: countUsers
        },
        "/user/signup": {
            post: postUser          
        },
        "user/confirm_account/{random_character}":{
            patch: confirmAccount
        },
        "/user/search": {
            post: searchUser
        },
        "/user/admin/create_author": {
            post: createAuthor
        },
        "/user/{id}": {
            patch: updateUser,
            get: getUser,
            delete: deleteUser
        },
        "/user/login": {
          post: loginUser
        },
        "user/forgot_password": {
            get: forgotPassword
        },
        "user/reset_password/{random_character}": {
            patch: resetPassword
        },
        "/user/logout": {
          get: logoutUser
        },
        "/package": {
            get: getPackages,
            post: postPackage
        },
        "/package/search": {
            post: searchPackage
        },
        "/package/{id}": {
          patch: updatePackage,
          get: getPackage,
          delete: deletePackage
        },
        "/review": {
            get: getReviews,
            post: postReview
        },
        "/review/search": {
            post: searchReview
        },
        "/review/{id}": {
          patch: updateReview,
          get: getReview,
          delete: deleteReview
        },
        "/category": {
            get: getCategories,
            post: postCategory
        },
        "/category/search": {
            post: searchCategory
        },
        "/category/{id}": {
          patch: updateCategory,
          get: getCategory,
          delete: deleteCategory
        },
        "/subscription": {
            get: getSubscriptions,
            post: postSubscription 
        },
        "/subscription/search": {
            post: searchSubscription
        },
        "/subscription/{id}": {
          patch: updateSubscription,
          get: getSubscription,
          delete: deleteSubscription
        },
        "/book": {
            get: getBooks,
            post: postBook
        },
        "/book/count": {
            get: countBooks
        },
        "/book/search": {
            post: searchBook
        },
        "/book/{id}": {
          patch: updateBook,
          get: getBook,
          delete: deleteBook
        },
        "/article": {
            get: getArticles,
            post: postArticle
        },
        "/article/count": {
            get: countArticles
        },
        "/article/search": {
            post: searchArticle
        },
        "/article/{id}": {
          patch: updateArticle,
          get: getArticle,
          delete: deleteArticle
        }
    },
    components: {
        securitySchemes: {
            bearerAuth: {
                type: "http",
                scheme: "bearer",
                bearerFormat: "JWT"
            }
        }
    },
    definitions
}
