const express = require('express');
const session = require('express-session');
const rateLimit = require('express-rate-limit');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
const hpp = require('hpp');
const cors = require('cors');

const user = require('./routes/userRoutes');
const subscription = require('./routes/subscriptionRoutes');
const review = require('./routes/reviewsRoutes');
const package = require('./routes/packagesRoutes');
const category = require('./routes/categoryRoutes');
const book = require('./routes/bookRoutes');
const article = require('./routes/articleRoutes');
const globalErrHandler = require('./controllers/middlewares/errorController');
const AppError = require('./utils/appError');
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger');
const dotenv = require('dotenv');


dotenv.config({
    path: './config.env'
});

var the = process.env.SESS_SECRET
const IN_PROD = process.env.NODE_ENV === 'production'

app.use(express.static('uploads'));
// Body parser, reading data from body into req.body
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(session({
    name: process.env.SESS_NAME,
    resave: false,
    saveUninitialized: false,
    secret: process.env.SESS_SECRET,
    cookie: {
        maxAge: 1000*60*60*2,
        sameSite: true,
        secure: IN_PROD
    }
}))

// Allow Cross-Origin requests
app.use(cors());

// Set security HTTP headers
app.use(helmet());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// Limit request from the same API 
const limiter = rateLimit({
    max: 150,
    windowMs: 60 * 60 * 1000,
    message: 'Too Many Request from this IP, please try again in an hour'
});
app.use('/api', limiter);

// Body parser, reading data from body into req.body
app.use(express.json({
    limit: '15kb'
}));

// Data sanitization against Nosql query injection
app.use(mongoSanitize());

// Data sanitization against XSS(clean user input from malicious HTML code)
app.use(xss());

// Prevent parameter pollution
app.use(hpp());


// Routes
app.use('/home', (req, res, next) =>{
    res.render('welcome to my home page');
    next();
});
app.use('/contant_us', (req, res, next) =>{
    res.render('contact us using the contacts below for more info');
    next();
})
app.use('/api/v1/user', user);
app.use('/api/v1/subscription', subscription);
app.use('/api/v1/review', review);
app.use('/api/v1/category', category);
app.use('/api/v1/package', package);
app.use('/api/v1/book', book);
app.use('/api/v1/article', article);

// handle undefined Routes
app.use('*', (req, res, next) => {
    const err = new AppError(404, 'fail', 'undefined route');
    next(err, req, res, next);
});

app.use(globalErrHandler);

module.exports = app;