exports.getReviews = {
    tags: ['Review'],
    summary: "Get All Reviews",
    description: "Returns all subscriptions from the system",
    operationId: 'getReviews',
    produces: [
      "application/xml",
      "application/json"
    ],
    responses: {
      200: {
        description: "successful operation",
        schema: {
          type: "string"
        },
      400: {
        description: "Invalid username/password supplied"
      }
    },
    security: [
      {
        bearerAuth: []
      }
    ]
  }
} 

exports.getReview = {
  tags: ["Review"],
  summary: "Get Review by id",
  operationId: "getReviewById",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "reviewId",
      in: "path",
      description: "The id of user's Review that needs to be fetched. ",
      required: true,
      type: "string"
    }
  ],
  responses: {
    200: {
      description: "successful operation",
      schema: {
        $ref: "#/definitions/Review"
      }
    },
    400: {
      description: "Invalid Review Id supplied"
    },
    404: {
      description: "Review Id not found"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.postReview = {
    tags: ['Review'],
      summary: "Create Review",
      description: "Create a new Review.",
      operationId: "createReview",
      produces: [
        "application/xml",
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Created Review object",
          required: true,
          schema: {
            $ref: "#/definitions/Review"
          }
        }
      ],
      responses: {
        default: {
          description: "successful operation"
        }
      },
      security: [
        {
          bearerAuth: []
        }
      ]
}

exports.searchReview = {
    tags: ['Review'],
      summary: "Search Review",
      description: "Search the database.",
      operationId: "searchReview",
      produces: [
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "found searched document",
          required: true,
            schema: {
              properties: {
                query: {
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: ["-1, 1"]
                    }
                  }
                }
              }
            }
        }
      ],
      responses: {
        200: {
          description: "successful operation",
          schema: {
            $ref: "#/definitions/ApiResponse"
          }
        }
      }
    }

exports.updateReview = {
  tags: ["Review"],
  summary: "Update Review",
  description: "This can only be done by the logged in user.",
  operationId: "updateReview",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "reviewId",
      in: "path",
      description: "id of Review that needs to be updated",
      required: true,
      type: "string"
    },
    {
      in: "body",
      name: "body",
      description: "Updated Review object",
      required: true,
      schema: {
        $ref: "#/definitions/Review"
      }
    }
  ],
  responses: {
    400: {
      description: "Invalid Review id supplied",
    },
    404: {
      description: "Review id not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.deleteReview = {
  tags: ["Review"],
  summary: "Delete Review",
  description: "This can only be done by the logged in user.",
  operationId: "deleteReview",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "reviewId",
      in: "path",
      description: "id of Review that needs to be deleted",
      required: true,
      type: "string"
    }
  ],
  responses: {
    400: {
      description: "Invalid Review id supplied",
    },
    404: {
      description: "Review not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}