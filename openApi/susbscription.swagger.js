exports.getSubscriptions = {
    tags: ['Subscription'],
    summary: "Get All subscriptions",
    description: "Returns all subscriptions from the system",
    operationId: 'getSubscriptions',
    produces: [
      "application/xml",
      "application/json"
    ],
    responses: {
      200: {
        description: "successful operation",
        schema: {
          type: "string"
        },
      400: {
        description: "Invalid username/password supplied"
      }
    },
    security: [
      {
        bearerAuth: []
      }
    ]
  }
} 

exports.getSubscription = {
  tags: ["Subscription"],
  summary: "Get subscription by id",
  operationId: "getSubscriptionById",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "subscriptionId",
      in: "path",
      description: "The id of user's subcription that needs to be fetched. ",
      required: true,
      type: "string"
    }
  ],
  responses: {
    200: {
      description: "successful operation",
      schema: {
        $ref: "#/definitions/Subscription"
      }
    },
    400: {
      description: "Invalid SubscriptionId supplied"
    },
    404: {
      description: "SubscriptionId not found"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.postSubscription = {
    tags: ['Subscription'],
      summary: "Create subscription",
      description: "Create a new subscription.",
      operationId: "createSubscription",
      produces: [
        "application/xml",
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Created subscription object",
          required: true,
          schema: {
            $ref: "#/definitions/Subscription"
          }
        }
      ],
      responses: {
        default: {
          description: "successful operation"
        }
      },
      security: [
        {
          bearerAuth: []
        }
      ]
}

exports.searchSubscription = {
    tags: ['Subscription'],
      summary: "Search Subscription",
      description: "Search the database.",
      operationId: "searchSubscription",
      produces: [
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "found searched document",
          required: true,
            schema: {
              properties: {
                query: {
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: ["-1, 1"]
                    }
                  }
                }
              }
            }
        }
      ],
      responses: {
        200: {
          description: "successful operation",
          schema: {
            $ref: "#/definitions/ApiResponse"
          }
        }
      }
    }

exports.updateSubscription = {
  tags: ["Subscription"],
  summary: "Update Subscription",
  description: "This can only be done by the logged in user.",
  operationId: "updateSubscription",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "subscriptionId",
      in: "path",
      description: "id of Subscription that needs to be updated",
      required: true,
      type: "string"
    },
    {
      in: "body",
      name: "body",
      description: "Updated user object",
      required: true,
      schema: {
        $ref: "#/definitions/Subscription"
      }
    }
  ],
  responses: {
    400: {
      description: "Invalid Subscription id supplied",
    },
    404: {
      description: "Subscription id not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.deleteSubscription = {
  tags: ["Subscription"],
  summary: "Delete Subscription",
  description: "This can only be done by the logged in user.",
  operationId: "deleteSubscription",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "subscriptionId",
      in: "path",
      description: "id of Subscription that needs to be deleted",
      required: true,
      type: "string"
    }
  ],
  responses: {
    400: {
      description: "Invalid Subscription id supplied",
    },
    404: {
      description: "Subscription not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}