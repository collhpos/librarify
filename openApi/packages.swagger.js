exports.getPackages = {
    tags: ['Package'],
    summary: "Get All Packages",
    description: "Returns all Packages from the system",
    operationId: 'getPackages',
    produces: [
      "application/xml",
      "application/json"
    ],
    responses: {
      200: {
        description: "successful operation",
        schema: {
          type: "string"
        },
      400: {
        description: "Invalid username/password supplied"
      }
    },
    security: [
      {
        bearerAuth: []
      }
    ]
  }
} 

exports.getPackage = {
  tags: ["Package"],
  summary: "Get Package by id",
  operationId: "getPackageById",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "packageId",
      in: "path",
      description: "The id of user's Package that needs to be fetched. ",
      required: true,
      type: "string"
    }
  ],
  responses: {
    200: {
      description: "successful operation",
      schema: {
        $ref: "#/definitions/Package"
      }
    },
    400: {
      description: "Invalid Package Id supplied"
    },
    404: {
      description: "Package Id not found"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.postPackage = {
    tags: ['Package'],
      summary: "Create Package",
      description: "Create a new Package.",
      operationId: "createPackage",
      produces: [
        "application/xml",
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Created Package object",
          required: true,
          schema: {
            $ref: "#/definitions/Package"
          }
        }
      ],
      responses: {
        default: {
          description: "successful operation"
        }
      },
      security: [
        {
          bearerAuth: []
        }
      ]
}

exports.searchPackage = {
    tags: ['Package'],
      summary: "Search Package",
      description: "Search the database.",
      operationId: "searchPackage",
      produces: [
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "found searched document",
          required: true,
            schema: {
              properties: {
                query: {
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: ["-1, 1"]
                    }
                  }
                }
              }
            }
        }
      ],
      responses: {
        200: {
          description: "successful operation",
          schema: {
            $ref: "#/definitions/ApiResponse"
          }
        }
      }
    }

exports.updatePackage = {
  tags: ["Package"],
  summary: "Update Package",
  description: "This can only be done by the logged in user.",
  operationId: "updatePackage",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "packageId",
      in: "path",
      description: "id of Package that needs to be updated",
      required: true,
      type: "string"
    },
    {
      in: "body",
      name: "body",
      description: "Updated Package object",
      required: true,
      schema: {
        $ref: "#/definitions/Package"
      }
    }
  ],
  responses: {
    400: {
      description: "Invalid Package id supplied",
    },
    404: {
      description: "Package id not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.deletePackage = {
  tags: ["Package"],
  summary: "Delete Package",
  description: "This can only be done by the logged in user.",
  operationId: "deletePackage",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "packageId",
      in: "path",
      description: "id of Package that needs to be deleted",
      required: true,
      type: "string"
    }
  ],
  responses: {
    400: {
      description: "Invalid Package id supplied",
    },
    404: {
      description: "Package not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}