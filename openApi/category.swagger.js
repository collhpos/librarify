exports.getCategories = {
    tags: ['Category'],
    summary: "Get All Categories",
    description: "Returns all Categories from the system",
    operationId: 'getCategories',
    produces: [
      "application/xml",
      "application/json"
    ],
    responses: {
      200: {
        description: "successful operation",
        schema: {
          type: "string"
        },
      400: {
        description: "Invalid username/password supplied"
      }
    },
    security: [
      {
        bearerAuth: []
      }
    ]
  }
} 

exports.getCategory = {
  tags: ["Category"],
  summary: "Get Category by id",
  operationId: "getCategoryById",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "categoryId",
      in: "path",
      description: "The id of user's Category that needs to be fetched. ",
      required: true,
      type: "string"
    }
  ],
  responses: {
    200: {
      description: "successful operation",
      schema: {
        $ref: "#/definitions/Category"
      }
    },
    400: {
      description: "Invalid Category Id supplied"
    },
    404: {
      description: "Category Id not found"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.postCategory = {
    tags: ['Category'],
      summary: "Create Category",
      description: "Create a new Category.",
      operationId: "createCategory",
      produces: [
        "application/xml",
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Created Category object",
          required: true,
          schema: {
            $ref: "#/definitions/Category"
          }
        }
      ],
      responses: {
        default: {
          description: "successful operation"
        }
      },
      security: [
        {
          bearerAuth: []
        }
      ]
}

exports.searchCategory = {
    tags: ['Category'],
      summary: "Search Category",
      description: "Search the database.",
      operationId: "searchCategory",
      produces: [
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "found searched document",
          required: true,
            schema: {
              properties: {
                query: {
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: ["-1, 1"]
                    }
                  }
                }
              }
            }
        }
      ],
      responses: {
        200: {
          description: "successful operation",
          schema: {
            $ref: "#/definitions/ApiResponse"
          }
        }
      }
    }

exports.updateCategory = {
  tags: ["Category"],
  summary: "Update Category",
  description: "This can only be done by the logged in user.",
  operationId: "updateCategory",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "categoryId",
      in: "path",
      description: "id of Category that needs to be updated",
      required: true,
      type: "string"
    },
    {
      in: "body",
      name: "body",
      description: "Updated Category object",
      required: true,
      schema: {
        $ref: "#/definitions/Category"
      }
    }
  ],
  responses: {
    400: {
      description: "Invalid Category id supplied",
    },
    404: {
      description: "Category id not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.deleteCategory = {
  tags: ["Category"],
  summary: "Delete Category",
  description: "This can only be done by the logged in user.",
  operationId: "deleteCategory",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "categoryId",
      in: "path",
      description: "id of Category that needs to be deleted",
      required: true,
      type: "string"
    }
  ],
  responses: {
    400: {
      description: "Invalid Category id supplied",
    },
    404: {
      description: "Category not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}