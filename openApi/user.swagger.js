exports.getUsers = {
    tags: ['User'],
    summary: "Get All users",
    description: "Returns all users from the system",
    operationId: 'getUsers',
    produces: [
      "application/xml",
      "application/json"
    ],
    responses: {
      200: {
        description: "success"
      },
      400: {
        description: "error"
      }
    },
    security: [
      {
        bearerAuth: []
      }
    ]
};

exports.countUsers = {
    tags: ['User'],
    summary: "count users",
    description: "Returns the number of users from the system",
    operationId: 'countUsers',
    produces: [
      "application/xml",
      "application/json"
    ],
    responses: {
      200: {
        description: "success",
        schema: {
          type: "number"
        }
      },
      200: {
        description: "This storage is empty"
      },
      400: {
        description: "error"
      }
    },
    security: [
      {
        bearerAuth: []
      }
    ]
};

exports.getUser = {
  tags: ["User"],
  summary: "Get user by id",
  operationId: "getUserById",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of user that needs to be fetched. ",
      required: true,
      type: "string"
    }
  ],
  responses: {
    200: {
      description: "successful operation",
    },
    400: {
      description: "Invalid username supplied"
    },
    404: {
      description: "User not found"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
};

exports.forgotPassword = {
  tags: ["User"],
  summary: "change user's password",
  operationId: "forgotPassword",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "email",
      in: "query",
      description: "The email of user that forgot password. ",
      required: true,
      type: "string"
    }
  ],
  responses: {
    200: {
      description: "A reset link has been sent to your email"
    },
    400: {
      description: "error"
    },
    404: {
      description: "The email does not exist"
    },
    404: {
      description: "The token provided does not exist"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
};

exports.postUser = {
    tags: ['User'],
      summary: "Create user",
      description: "Create a new user.",
      operationId: "createUser",
      produces: [
        "application/xml",
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Created user object",
          required: true,
          schema: {
            properties: {
              firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              },
              email: {
                type: "string"
              },
              password: {
                type: "string"
              },
              roleCode: {
                type: "string",
                description: "1 = admin, 2 = author, 3 = reader"
              }
            }
          }
        }
      ],
      responses: {
        200: {
          description: "success"
        },
        400: {
          description: "error"
        }
      }
}

exports.searchUser = {
    tags: ['User'],
      summary: "Search user",
      description: "Search the database.",
      operationId: "searchUser",
      produces: [
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "found searched document",
          required: true,
            schema: {
              properties: {
                query: {
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: ["-1, 1"]
                    }
                  }
                }
              }
            }
        }
      ],
      responses: {
        200: {
          description: "success"
        },
        400: {
          description: "error"
        }
      },
      security: [
        {
          bearerAuth: []
        }
      ]
    }

exports.updateUser = {
  tags: ["User"],
  summary: "Update user",
  description: "This can only be done by the logged in user.",
  operationId: "updateUser",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of user that needs to be updated",
      required: true,
      type: "string"
    },
    {
      in: "body",
      name: "body",
      description: "Updated user object",
      required: true,
      schema: {
        $ref: "#/definitions/User"
      }
    }
  ],
  responses: {
    200: {
      description: "updated"
    },
    400: {
      description: "error",
    },
    404: {
      description: "This user token doesn\'t exist",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
};

exports.confirmAccount = {
  tags: ["User"],
  summary: "confirm user account",
  description: "This can only be done by the logged in user.",
  operationId: "confirmAccount",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "confirmation_token",
      in: "path",
      description: "confirmation token sent to the user's email that needs to be updated",
      required: true,
      type: "string"
    },
    {
      in: "body",
      name: "body",
      description: "Updated user object",
      required: true,
      schema: {
        properties: {
          active: {
            type: "boolean",
            description: "all fields are done on default so just click on the link to update your profile"
          }
        }
      }
    }
  ],
  responses: {
    400: {
      description: "error",
    },
    404: {
      description: "This user token doesn\'t exist",
    },
    404: {
      description: "You are not authorized to perform this operation"
    },
    200: {
      description: "your account has been activated successfully"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
};

exports.resetPassword = {
  tags: ["User"],
  summary: "reset password",
  description: "This can only be done by the logged in user.",
  operationId: "resetPassword",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "remember_token",
      in: "path",
      description: "remember token for password reset",
      required: true,
      type: "string"
    },
    {
      in: "body",
      name: "body",
      description: "Updated user object",
      required: true,
      schema: {
        properties: {
          password: {
            type: "string"
          }
        }
      }
    }
  ],
  responses: {
    400: {
      description: "error"
    },
    404: {
      description: "Password field is required"
    },
    404: {
      description: "You are not authorized to perform this operation"
    },
    404: {
      description: "This user token doesn\'t exist"
    },
    200: {
      description: "your password has been reset successfully"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
};

exports.createAuthor = {
    tags: ['User'],
      summary: "Create author",
      description: "Create an author.",
      operationId: "createAuthor",
      produces: [
        "application/xml",
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Created user object",
          required: true,
          schema: {
            properties: {
              firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              },
              email: {
                type: "string"
              },
              password: {
                type: "string"
              },
              phone: {
                type: "string"
              },
              roleCode: {
                type: "string",
                description: "1 = admin, 2 = author, 3 = reader"
              }
            }
          }
        }
      ],
      responses: {
        200: {
          description: "your account has been activated successfully"
        },
        400: {
          description: "error"
        }
      }
};

exports.deleteUser = {
  tags: ["User"],
  summary: "Delete user",
  description: "This can only be done by the logged in user.",
  operationId: "deleteUser",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of user that needs to be deleted",
      required: true,
      type: "string"
    }
  ],
  responses: {
    200: {
      description: "This document has been deleted"
    },
    400: {
      description: "error",
    },
    404: {
      description: "No document found with that id",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.loginUser = {
  tags: ['User'],
      summary: "login a user",
      description: "login a new user.",
      operationId: "loginUser",
      produces: [
        "application/xml",
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Created login object",
          required: true,
          schema: {
            properties: {
              email: {
                type: "string"
              },
              password: {
                type: "string"
              },
              roleCode: {
                type: "number"
              }
            }
          }
        }
      ],
      responses: {
        200: {
          description: "success"
        },
        400: {
          description: "error"
        },
        404: {
          description: "Email or Password is wrong"
        },
        404: {
          description: "The role inputted doesn\'t match the role used in creating this account"
        }
      }
}

exports.logoutUser = {
  tags: ["User"],
  summary: "Logs out current logged in user session",
  operationId: "logoutUser",
  produces: [
    "application/xml",
    "application/json"
  ],
  responses: {
    200: {
      description: "you have successfully logged out of your account"
    },
    400: {
      description: "error"
    },
    404: {
      description: "unable to clear cookie from this account"
    },
    404: {
      description: "unable to logout from this account"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}
