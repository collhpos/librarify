exports.getBooks = {
    tags: ['Book'],
    summary: "Get All Books",
    description: "Returns all Books from the system",
    operationId: 'getBooks',
    produces: [
      "application/xml",
      "application/json"
    ],
    responses: {
      200: {
        description: "successful operation",
        schema: {
          type: "string"
        },
      400: {
        description: "Invalid username/password supplied"
      }
    },
    security: [
      {
        bearerAuth: []
      }
    ]
  }
};

exports.countBooks = {
    tags: ['Book'],
    summary: "Get books",
    description: "Returns the number of books from the system",
    operationId: 'countBooks',
    produces: [
      "application/xml",
      "application/json"
    ],
    responses: {
      200: {
        description: "successful operation",
        schema: {
          type: "number"
        }
      },
      400: {
        description: "Invalid username/password supplied"
      }
    },
    security: [
      {
        bearerAuth: []
      }
    ]
};

exports.getBook = {
  tags: ["Book"],
  summary: "Get Book by id",
  operationId: "getBookById",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "bookId",
      in: "path",
      description: "The id of user's Book that needs to be fetched. ",
      required: true,
      type: "string"
    }
  ],
  responses: {
    200: {
      description: "successful operation",
      schema: {
        $ref: "#/definitions/Book"
      }
    },
    400: {
      description: "Invalid Book Id supplied"
    },
    404: {
      description: "Book Id not found"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.postBook = {
    tags: ['Book'],
      summary: "Create Book",
      description: "Create a new Book.",
      operationId: "createBook",
      produces: [
        "application/xml",
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Created Book object",
          required: true,
          schema: {
            $ref: "#/definitions/Book"
          }
        }
      ],
      responses: {
        default: {
          description: "successful operation"
        }
      },
      security: [
        {
          bearerAuth: []
        }
      ]
}

exports.searchBook = {
    tags: ['Book'],
      summary: "Search Book",
      description: "Search the database.",
      operationId: "searchBook",
      produces: [
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "found searched document",
          required: true,
            schema: {
              properties: {
                query: {
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: ["-1, 1"]
                    }
                  }
                }
              }
            }
        }
      ],
      responses: {
        200: {
          description: "successful operation",
          schema: {
            $ref: "#/definitions/ApiResponse"
          }
        }
      }
    }

exports.updateBook = {
  tags: ["Book"],
  summary: "Update Book",
  description: "This can only be done by the logged in user.",
  operationId: "updateBook",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "bookId",
      in: "path",
      description: "id of Book that needs to be updated",
      required: true,
      type: "string"
    },
    {
      in: "body",
      name: "body",
      description: "Updated Book object",
      required: true,
      schema: {
        $ref: "#/definitions/Book"
      }
    }
  ],
  responses: {
    400: {
      description: "Invalid Book id supplied",
    },
    404: {
      description: "Book id not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.deleteBook = {
  tags: ["Book"],
  summary: "Delete Book",
  description: "This can only be done by the logged in user.",
  operationId: "deleteBook",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "bookId",
      in: "path",
      description: "id of Book that needs to be deleted",
      required: true,
      type: "string"
    }
  ],
  responses: {
    400: {
      description: "Invalid Book id supplied",
    },
    404: {
      description: "Book not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}