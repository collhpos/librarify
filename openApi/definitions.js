exports.definitions = {
  User: {
    type: 'object',
    properties: {               
      firstname: {
        type: "string"
      },
      lastname: {
        type: "string"
      },
      email: {
        type: "string"
      },
      password: {
        type: "string"
      },
      phone: {
        type: "string"
      },
      roleCode: {
        type: "string",
        description: "1 = admin, 2 = author, 3 = reader"
      },
      confirmation_token: {
        type: "string"
      },
      remember_token: {
        type: "string"
      }
    },
    xml: {
      "name": "User"
    }
  },  
  Subscription: {
    type: 'object',
      properties: {               
        package_id: {
          type: "string"
        },
        user_id: {
          type: "string"
        },
        start_date: {
          type: "string"
        },
        endDate: {
          type: "string"
        },  
        active: {
          type: "boolean",
          description: "subscription Status",
          enum: [true, false]
        }
      }
  },  
  Review: {
    type: 'object',
      properties: { 
        user_id: {
          type: "string"
        },              
        comment: {
          type: "string"
        },
        book_id: {
          type: "string",
        },
        rating: {
          type: "string",
          description: "0, 1, 2, ...."
        }
      }
  },
  Package: {
    type: 'object',
      properties: {               
        name: {
          type: "string"
        },
        code: {
          type: "string"
        },
        amount: {
          type: "string",
        },
        duration: {
            type: "string"
        },
        description: {
            type: 'string'
        }
      }
  }, 
  Category: {
    type: 'object',
      properties: {               
        name: {
          type: "mixed"
        },
        code: {
          type: "mixed"
        }
      }
  }, 
  Book: {
    type: 'object',
      properties: {               
        title: {
          type: "string"
        },
        category_id: {
          type: "string"
        },
        year: {
          type: "string",
        },
        pages: {
            type: "number"
        },
        excerpt: {
            type: "string"
        },
        views: {
            type: "string"
        },
        author: {
            type: "string"
        },
        user_id: {
            type: "string"
        },
        rating: {
            type: "string",
            description: "0, 1, 2, ...."
        },
        book_url: {
            type: "string"
        },
        thumbnail: {
            type: "string"
        },
        publish: {
            type: "boolean"
        }
      }
  }, 
  Article: {
    type: 'object',
      properties: {               
        title: {
          type: "string"
        },
        category_id: {
          type: "string"
        },
        body: {
          type: "string",
        },
        views: {
            type: "string"
        },
        user_id: {
            type: "string"
        },
        rating: {
            type: "string",
            description: "0, 1, 2, ...."
        },
        thumbnail: {
            type: "buffer"
        },
        publish: {
            type: "boolean"
        }
      }
  },
  ApiResponse: {
    type: "object",
    properties: {
      code: {
        type: "integer",
        format: "int32"
      },
      type: {
        type: "string"
      },
      message: {
        type: "string"
      }
    }
  }     
};