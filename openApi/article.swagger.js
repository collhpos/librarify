exports.getArticles = {
    tags: ['Article'],
    summary: "Get All Articles",
    description: "Returns all Articles from the system",
    operationId: 'getArticles',
    produces: [
      "application/xml",
      "application/json"
    ],
    responses: {
      200: {
        description: "successful operation",
        schema: {
          type: "string"
        },
      400: {
        description: "Invalid username/password supplied"
      }
    },
    security: [
      {
        bearerAuth: []
      }
    ]
  }
};

exports.countArticles = {
    tags: ['Article'],
    summary: "Get articles",
    description: "Returns the number of articles from the system",
    operationId: 'countArticles',
    produces: [
      "application/xml",
      "application/json"
    ],
    responses: {
      200: {
        description: "successful operation",
        schema: {
          type: "number"
        }
      },
      400: {
        description: "Invalid username/password supplied"
      }
    },
    security: [
      {
        bearerAuth: []
      }
    ]
};

exports.getArticle = {
  tags: ["Article"],
  summary: "Get Article by id",
  operationId: "getArticleById",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "articleId",
      in: "path",
      description: "The id of user's Article that needs to be fetched. ",
      required: true,
      type: "string"
    }
  ],
  responses: {
    200: {
      description: "successful operation",
      schema: {
        $ref: "#/definitions/Article"
      }
    },
    400: {
      description: "Invalid Article Id supplied"
    },
    404: {
      description: "Article Id not found"
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.postArticle = {
    tags: ['Article'],
      summary: "Create Article",
      description: "Create a new Article.",
      operationId: "createArticle",
      produces: [
        "application/xml",
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Created Article object",
          required: true,
          schema: {
            $ref: "#/definitions/Article"
          }
        }
      ],
      responses: {
        default: {
          description: "successful operation"
        }
      },
      security: [
        {
          bearerAuth: []
        }
      ]
}

exports.searchArticle = {
    tags: ['Article'],
      summary: "Search Article",
      description: "Search the database.",
      operationId: "searchArticle",
      produces: [
        "application/json"
      ],
      parameters: [
        {
          in: "body",
          name: "body",
          description: "found searched document",
          required: true,
            schema: {
              properties: {
                query: {
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: ["-1, 1"]
                    }
                  }
                }
              }
            }
        }
      ],
      responses: {
        200: {
          description: "successful operation",
          schema: {
            $ref: "#/definitions/ApiResponse"
          }
        }
      }
    }

exports.updateArticle = {
  tags: ["Article"],
  summary: "Update Article",
  description: "This can only be done by the logged in user.",
  operationId: "updateArticle",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "articleId",
      in: "path",
      description: "id of Article that needs to be updated",
      required: true,
      type: "string"
    },
    {
      in: "body",
      name: "body",
      description: "Updated Article object",
      required: true,
      schema: {
        $ref: "#/definitions/Article"
      }
    }
  ],
  responses: {
    400: {
      description: "Invalid Article id supplied",
    },
    404: {
      description: "Article id not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}

exports.deleteArticle = {
  tags: ["Article"],
  summary: "Delete Article",
  description: "This can only be done by the logged in user.",
  operationId: "deleteArticle",
  produces: [
    "application/xml",
    "application/json"
  ],
  parameters: [
    {
      name: "articleId",
      in: "path",
      description: "id of Article that needs to be deleted",
      required: true,
      type: "string"
    }
  ],
  responses: {
    400: {
      description: "Invalid Article id supplied",
    },
    404: {
      description: "Article not found",
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
}